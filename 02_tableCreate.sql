
--creating tables
create table Superhero (
	id int PRIMARY KEY IDENTITY(1,1),
	[Name] varchar(50) not null,
	Alias varchar(50),
	Origin varchar(50)
	)

create table Assistant (
	id int PRIMARY KEY IDENTITY (1,1),
	[Name] varchar(50) not null,
	)

create table [Power] (
	id int PRIMARY KEY IDENTITY (1,1),
	[Name] varchar(50) not null,
	[Description] varchar(50)
	)
	

