
--adding foreign key of superhero_id in assistant table because one superhero can have many assistants
ALTER TABLE assistant ADD superhero_id INT not null
ALTER TABLE assistant ADD CONSTRAINT fk_superhero FOREIGN KEY (superhero_id) REFERENCES superhero(id)
 
--creating linking table for the many to many relationship between superhero and power. Adding foreign keys for each of the corresponding primary keys in the linking table.
CREATE TABLE superhero_power (
s_id INT, 
p_id INT,
CONSTRAINT s_p PRIMARY KEY (s_id,p_id))

ALTER TABLE superhero_power
ADD FOREIGN KEY (s_id) REFERENCES superhero(id)

ALTER TABLE superhero_power
ADD FOREIGN KEY (p_id) REFERENCES [power](id)