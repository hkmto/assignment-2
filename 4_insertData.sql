
--inserting data into superhero table
INSERT INTO superhero ([name],alias,origin ) VALUES ('Morten Tobias', 'Warlord', 'Norway')
INSERT INTO superhero ([name],alias,origin ) VALUES ('Sean Spicer', 'CringeGoat', 'Jupiter')
INSERT INTO superhero ([name],alias,origin ) VALUES ('Warren Buffet', 'Monastary', 'Moon')

--inserting data into assistant table

INSERT INTO assistant ([name], superhero_id) VALUES ('Vladimir Putin',1)
INSERT INTO assistant ([name], superhero_id) VALUES ('Zelensky',1)
INSERT INTO assistant ([name], superhero_id) VALUES ('Klitskoj',2)

-- inserting data into power table

INSERT INTO [power] ([name], [description]) VALUES ('Casting magic spells','crippling superpower')
INSERT INTO [power] ([name], [description]) VALUES ('flying','getaway power')
INSERT INTO [power] ([name], [description]) VALUES ('magic mushrooms','total mayhem')
INSERT INTO [power] ([name], [description]) VALUES ('Sleep spell','sleeping forever')

-- inserting data into linking table

INSERT INTO superhero_power (s_id, p_id) VALUES (1,2)
INSERT INTO superhero_power (s_id, p_id) VALUES (1,3)
INSERT INTO superhero_power (s_id, p_id) VALUES (2,2)
INSERT INTO superhero_power (s_id, p_id) VALUES (3,2)







