﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignment2.Models
{
    public class CustomerGenre
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Genre { get; set; }
        public int CountGenre { get; set; } 
    }
}
