﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignment2
{
    public class Customer
    {
        public int CustId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
       
    }
}
