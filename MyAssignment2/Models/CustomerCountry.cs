﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignment2.Models
{
    public class CustomerCountry
    {
        public int CountCustomers { get; set; }
        public string Country { get; set; }
    }
}
