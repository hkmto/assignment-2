﻿using MyAssignment2.Data_Access;
using MyAssignment2.Models;
using System;
using System.Collections.Generic;

namespace MyAssignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerSpenderRepository customerSpenderRepository = new CustomerSpenderRepository();
            ICustomerCountryRepository customerCountryRepository = new CustomerCountryRepository();
            ICustomerGenreRepository customerGenreRepository = new CustomerGenreRepository();
            CustomerReportGenrerator customerReportGenrerator = new CustomerReportGenrerator();

            customerRepository.GetCustomerById(39);
            customerRepository.GetAllCustomers();
            customerRepository.GetCustomerByName("es");
            customerRepository.GetCustomerPage(10, 20);
            Customer newcustomer = new Customer { Firstname = "Morten", Lastname = "Tobias", Country = "Norge", PostalCode = "5122", PhoneNumber = "91898295", Email = "metoo@nhh.no" };
            Customer updatecustomer = new Customer { CustId = 17, Lastname = "Tobiasssen", Email = "noe@nhh.no" };

            customerRepository.InsertCustomer(newcustomer);
            customerRepository.UpdateCustomer(updatecustomer);


            customerReportGenrerator.PrintCustomer(customerRepository.GetCustomerPage(10, 20));
            customerReportGenrerator.PrintCustomerSpender(customerSpenderRepository.GetCustomerSpenders());
            customerReportGenrerator.PrintCustomerCountry(customerCountryRepository.GetCustomerCountries());
            customerReportGenrerator.PrintCustomerTopGenre(customerGenreRepository.GetTopCustomerGenre(5));

        }


    }

}
