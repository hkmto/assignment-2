﻿using MyAssignment2.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignment2
{
    public class CustomerReportGenrerator
    {

        public void PrintCustomerSpender(IEnumerable<CustomerSpender> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"Name: {item.FirstName},{item.LastName}");
                Console.WriteLine($"Total ammount spent: {item.Total}");
                Console.WriteLine($"..................................");
            }

        }

        public void PrintCustomer(IEnumerable<Customer> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"Name: {item.Firstname},{item.Lastname}");
                Console.WriteLine($"Country: {item.Country}");
                Console.WriteLine($"Postal code: {item.PostalCode}");
                Console.WriteLine($"Phone number: {item.PhoneNumber}");
                Console.WriteLine($"Email: {item.Email}");
                Console.WriteLine($"..................................");
            }

        }

       public void PrintCustomerCountry(IEnumerable<CustomerCountry> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"There is a total number of customers: {item.CountCustomers} from {item.Country}");
                Console.WriteLine($"..................................");
            }

        }
        public void PrintCustomerTopGenre(IEnumerable<CustomerGenre> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"Customer {item.FirstName},{item.LastName} prefers {item.Genre} and has a count of {item.CountGenre}");
                Console.WriteLine($"..................................");
            }

        }
    }
}
