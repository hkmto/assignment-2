﻿using MyAssignment2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace MyAssignment2.Data_Access
{
    class CustomerGenreRepository : ICustomerGenreRepository
    {
        public IEnumerable<CustomerGenre> GetTopCustomerGenre(int custid)
        {
            List<CustomerGenre> customers = new List<CustomerGenre>();

            try
            {
                using SqlConnection connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = " SELECT TOP(1) WITH TIES c.firstname,c.Lastname,g.[Name] AS Genre,COUNT (g.[Name]) AS CountGenre FROM customer c" +
                          " INNER JOIN invoice i ON i.CustomerId = c.CustomerId"+
                          " INNER JOIN InvoiceLine i2 ON i2.InvoiceId = i.InvoiceId" +
                          " INNER JOIN Track t ON t.TrackId = i2.TrackId" +
                          " INNER JOIN genre g ON g.GenreId = t.GenreId" +
                          $" WHERE c.CustomerId = {custid}" +
                          " GROUP BY c.FirstName,c.LastName, g.[Name]" +
                          " ORDER BY CountGenre DESC";


                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    CustomerGenre customergenre = new CustomerGenre
                    {
                        FirstName = reader.GetString(0),
                        LastName = reader.GetString(1),
                        Genre= reader.GetString(2),
                        CountGenre= reader.GetInt32(3)

                    };

                    customers.Add(customergenre);

                }


            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


            return customers;
        }

      
    }
}
