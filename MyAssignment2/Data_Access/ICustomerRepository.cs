﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace MyAssignment2.Data_Access
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAllCustomers();
        Customer GetCustomerById(int CustId);
        Customer GetCustomerByName(string lastname);
        IEnumerable<Customer> GetCustomerPage(int limit, int offset);
        void InsertCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
            
    }
}
