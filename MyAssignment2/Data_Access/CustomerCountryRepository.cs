﻿using MyAssignment2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace MyAssignment2.Data_Access
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        public IEnumerable<CustomerCountry> GetCustomerCountries()
        {
            List<CustomerCountry> customers = new List<CustomerCountry>();

            try
            {
                using SqlConnection connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = "SELECT COUNT(customerid)AS CountCustomers, country FROM Customer GROUP BY country ORDER BY CountCustomers DESC";


                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    CustomerCountry customercountry = new CustomerCountry
                    {
                        CountCustomers = reader.GetInt32(0),
                        Country = reader.GetString(1),
                       
                    };

                    customers.Add(customercountry);

                }



            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


            return customers;
        }
    }
}
