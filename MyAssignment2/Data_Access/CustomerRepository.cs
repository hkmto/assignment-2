﻿using MyAssignment2.Data_Access;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MyAssignment2
{
    public class CustomerRepository : ICustomerRepository
    {

        public IEnumerable<Customer> GetAllCustomers()
        {
            //int columnPosition = myReader.GetOrdinal("columnName");
            //string columnValue = myReader.IsDBNull(columnPosition) ? string.Empty : myReader.GetString("columnName");

            List<Customer> customers = new List<Customer>();
   
            try
            {
                using SqlConnection connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = "Select CustomerId, FirstName, LastName, ISNULL(Country,''), ISNULL(PostalCode,''),ISNULL(Phone,''),ISNULL(Email,'')from Customer";

                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                
                    Customer customer = new Customer
                    {
                        CustId = reader.GetInt32(0),
                        Firstname = reader.GetString(1),
                        Lastname = reader.GetString(2),
                        Country = reader.GetString(3),
                        PostalCode = reader.GetString(4),
                        PhoneNumber = reader.GetString(5),
                        Email = reader.GetString(6)
                 
                    };

                    customers.Add(customer);
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return customers;
        }

        public Customer GetCustomerById(int CustId)
        {
            var _customer = new Customer();

            try
            {
                using SqlConnection connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = $"Select CustomerId, FirstName, LastName, ISNULL(Country, ''), ISNULL(PostalCode, ''), ISNULL(Phone, ''), ISNULL(Email, '') from Customer where CustomerId={CustId}";

                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                reader.Read();
                Customer customer = new Customer
                {
                    CustId = reader.GetInt32(0),
                    Firstname = reader.GetString(1),
                    Lastname = reader.GetString(2),
                    Country = reader.GetString(3),
                    PostalCode = reader.GetString(4),
                    PhoneNumber = reader.GetString(5),
                    Email = reader.GetString(6)
                };
                

                _customer = customer;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return _customer;
        }

        public Customer GetCustomerByName(string lastname)
        {
            var _customer = new Customer();

            try
            {
                using SqlConnection connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = $"Select CustomerId, FirstName, LastName, ISNULL(Country, ''), ISNULL(PostalCode, ''), ISNULL(Phone, ''), ISNULL(Email, '') from Customer where Lastname like '%{lastname}%'";

                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                reader.Read();
                Customer customer = new Customer
                {
                    CustId = reader.GetInt32(0),
                    Firstname = reader.GetString(1),
                    Lastname = reader.GetString(2),
                    Country = reader.GetString(3),
                    PostalCode = reader.GetString(4),
                    PhoneNumber = reader.GetString(5),
                    Email = reader.GetString(6)
                };

              _customer = customer;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return _customer;
        }

        public IEnumerable<Customer> GetCustomerPage(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using SqlConnection connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = "Select CustomerId, FirstName, LastName, ISNULL(Country, ''), ISNULL(PostalCode, ''),ISNULL(Phone, ''),ISNULL(Email, '')from Customer "+
                          $" order by CustomerId OFFSET {limit} rows Fetch next {offset} rows only";

                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    Customer customer = new Customer
                    {
                        CustId = reader.GetInt32(0),
                        Firstname = reader.GetString(1),
                        Lastname = reader.GetString(2),
                        Country = reader.GetString(3),
                        PostalCode = reader.GetString(4),
                        PhoneNumber = reader.GetString(5),
                        Email = reader.GetString(6)
                    };

                    customers.Add(customer);
      
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return customers;
        }

        public void InsertCustomer(Customer customer)
        {
            try
            {
                using var connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = "INSERT INTO Customer (FirstName,LastName,Country,PostalCode,Phone,Email) values (@first_name,@last_name,@country,@postal_code,@phone_number,@email)";
                var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@first_name", customer.Firstname);
                command.Parameters.AddWithValue("@last_name", customer.Lastname);
                command.Parameters.AddWithValue("@country", customer.Country);
                command.Parameters.AddWithValue("@postal_code", customer.PostalCode);
                command.Parameters.AddWithValue("@phone_number", customer.PhoneNumber);
                command.Parameters.AddWithValue("@email", customer.Email);
           

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
          
        }

        public void UpdateCustomer(Customer customer) //updates an existing customers lastname and mail
        {
            try
            {
                using var connection = new SqlConnection(DbConfig.Config());
                connection.Open();
                var sql = $"UPDATE Customer SET lastname=@newLastName,email=@newEmail WHERE customerid={customer.CustId}";
                var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@newLastName", customer.Lastname);
                command.Parameters.AddWithValue("@newEmail", customer.Email);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
    }

}
