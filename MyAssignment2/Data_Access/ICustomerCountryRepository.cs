﻿using MyAssignment2.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignment2.Data_Access
{
    public interface ICustomerCountryRepository
    {
        IEnumerable<CustomerCountry> GetCustomerCountries(); 
    }
}
