﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace MyAssignment2.Data_Access
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        public IEnumerable<CustomerSpender> GetCustomerSpenders()
        {
            //int columnPosition = myReader.GetOrdinal("columnName");
            //string columnValue = myReader.IsDBNull(columnPosition) ? string.Empty : myReader.GetString("columnName");

            List<CustomerSpender> customers = new List<CustomerSpender>();

            try
            {
                using SqlConnection connection = new SqlConnection(DbConfig.Config());
                connection.Open();

                var sql = "select c.FirstName,c.Lastname,sum(Total) as TotalSpent from Customer c" +
                          " inner join Invoice i on c.CustomerId = i.CustomerId" +
                          " group by FirstName,Lastname,c.CustomerId order by sum(total) desc";


                using SqlCommand command = new SqlCommand(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    CustomerSpender customer = new CustomerSpender
                    {
                        FirstName = reader.GetString(0),
                        LastName=reader.GetString(1),
                        Total=reader.GetDecimal(2)

                    };

                    customers.Add(customer);

                }



            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


            return customers;
        }
    }
}
